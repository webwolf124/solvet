jQuery(document).ready(function($) {
  tinymce.PluginManager.add('custom_shortcode', function( editor, url ) { // id кнопки true_mce_button должен быть везде один и тот же
    editor.addButton( 'custom_shortcode', { // id кнопки true_mce_button
      icon: 'perec', // мой собственный CSS класс, благодаря которому я задам иконку кнопки
      text: 'Кастомные шорткоды',
      type: 'menubutton',
      title: 'Вставить элемент', // всплывающая подсказка при наведении
      menu: [
        {
          text: 'Блок акции',
          onclick: function() {
            editor.windowManager.open( {
              title: 'Задайте параметры',
              body: [
                {
                  type: 'textbox', // тип textbox = текстовое поле
                  name: 'title', // ID, будет использоваться ниже
                  label: 'Заголовок', // лейбл
                },
                {
                  type: 'listbox', // тип listbox = выпадающий список select
                  name: 'count',
                  label: 'Количество записей',
                  values: [ // значения выпадающего списка
                    {text: '1', value: '1'}, // лейбл, значение
                    {text: '2', value: '2'},
                    {text: '3', value: '3'},
                    {text: '4', value: '4'},
                    {text: '5', value: '5'},
                    {text: '6', value: '6'},
                    {text: '7', value: '7'},
                    {text: '8', value: '8'},
                    {text: '9', value: '9'},
                    {text: '10', value: '10'},
                    {text: '11', value: '11'},
                    {text: '12', value: '12'},
                  ]
                },
                {
                  type: 'textbox', // тип textbox = текстовое поле
                  name: 'button_text', // ID, будет использоваться ниже
                  label: 'Текст кнопки', // лейбл
                },
                {
                  type: 'textbox', // тип textbox = текстовое поле
                  name: 'button_link', // ID, будет использоваться ниже
                  label: 'Ссылка кнопки', // лейбл
                },
              ],
              onsubmit: function( e ) { // это будет происходить после заполнения полей и нажатии кнопки отправки
                editor.insertContent( '[shc_sell title="' + e.data.title + '" showposts="' + e.data.count + '" button_text="' + e.data.button_text + '" button_link="' + e.data.button_link + '"]');
              }
            });
          }
        },
        {
          text: 'Блок новости',
          onclick: function() {
            editor.windowManager.open( {
              title: 'Задайте параметры',
              body: [
                {
                  type: 'textbox', // тип textbox = текстовое поле
                  name: 'title', // ID, будет использоваться ниже
                  label: 'Заголовок', // лейбл
                },
                {
                  type: 'listbox', // тип listbox = выпадающий список select
                  name: 'count',
                  label: 'Количество записей',
                  values: [ // значения выпадающего списка
                    {text: '1', value: '1'}, // лейбл, значение
                    {text: '2', value: '2'},
                    {text: '3', value: '3'},
                    {text: '4', value: '4'},
                    {text: '5', value: '5'},
                    {text: '6', value: '6'},
                    {text: '7', value: '7'},
                    {text: '8', value: '8'},
                    {text: '9', value: '9'},
                    {text: '10', value: '10'},
                    {text: '11', value: '11'},
                    {text: '12', value: '12'},
                  ]
                },
                {
                  type: 'textbox', // тип textbox = текстовое поле
                  name: 'button_text', // ID, будет использоваться ниже
                  label: 'Текст кнопки', // лейбл
                },
                {
                  type: 'textbox', // тип textbox = текстовое поле
                  name: 'button_link', // ID, будет использоваться ниже
                  label: 'Ссылка кнопки', // лейбл
                },
              ],
              onsubmit: function( e ) { // это будет происходить после заполнения полей и нажатии кнопки отправки
                editor.insertContent( '[shc_news title="' + e.data.title + '" showposts="' + e.data.count + '" button_text="' + e.data.button_text + '" button_link="' + e.data.button_link + '"]');
              }
            });
          }
        },
      ]
    });
  });
});
