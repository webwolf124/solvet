import $ from 'jquery';
import '@fancyapps/fancybox';
import Vue from 'vue/dist/vue.js';
import axios from 'axios';
import Select2 from 'v-select2-component';

const btnMenu     = $('.btn-menu'),
      headerMenu  = $('.site-header__main-navigation');

btnMenu.on('click', function(e) {
  e.preventDefault();

  $(this).toggleClass('active');
  headerMenu.slideToggle('active');
})

const childrenItem = $('.menu-item-has-children');

$('> a', childrenItem).after('<span class="child-dropdown"></span>');
$('.child-dropdown').on('click', function() {
  $('+ .sub-menu', this).slideToggle();
})

let itemLogin = $('#primary-menu li.login a'),
    textLogin = itemLogin.text();

    itemLogin.html('<img class="login-image" src="/app/themes/webwolf/src/images/icon-login-solvent.svg" />' + textLogin );

/**
 * Stella
 */
new Vue({
  el: '#stella',
  components: {
    Select2
  },
  data() {
    return {
      change: false,
      selected: {
        id: '',
        text: '',
        acf: [],
        image: ''
      },
      value: '',
      options: [
        {
          id: '',
          text: '',
          acf: [],
          image: ''
        }
      ]
    };
  },
  mounted() {
    axios
      .get('/wp-json/wp/v2/stella')
      .then((response) => {
        let items = response.data;
        let options = [];

        let noimage = {
          source_url: '/app/themes/webwolf/src/images/no_image_w_large.png',
          alt_text: 'no image'
        }
        
        items.map(function(value, key) {
          options.push({
            id: value.id,
            text: value.title.rendered,
            acf: value.acf,
            image: value.better_featured_image ? value.better_featured_image : noimage
          })
        })

        this.value = options[0].id;
        this.options = options;
        this.selected = options[0];
      });
  },
  methods: {
    selectItem: function(item) {
      this.change = true; 

      setTimeout(function () {

        this.selected = item

        this.change = false; 

      }.bind(this), 1500)

    },
    changeItem: function(item) {
    }
 }
});
