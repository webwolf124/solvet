<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package webwolf
 */

?>

	<footer class="site-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<a href="<?php esc_url( home_url() ) ?>" class="header-logo">
						<?php
						$header_logo = get_theme_mod('header_logo');
						$img = wp_get_attachment_image_src($header_logo, 'full');
						if ($img) :
							?>
							<img src="<?php echo $img[0]; ?>" alt="">
						<?php endif; ?>
					</a>
					<!-- Описание сайта -->
					<?php
					$webwolf_description = get_bloginfo( 'description', 'display' );
					if ( $webwolf_description || is_customize_preview() ) :
					?>
						<p class="site-description"><?php echo $webwolf_description; /* WPCS: xss ok. */ ?></p>
					<?php endif; ?>
					<!-- /Описание сайта -->
				</div>
				<div class="col-md-5">
					<nav class="site-footer__main-navigation">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer',
								'menu_id'        => 'primary-menu',
							) );
						?>
					</nav>
				</div>
				<div class="col-md-3">
					<div class="site-footer__label"><?php esc_html_e( 'Главный офис', 'webwolf' ); ?></div>
					<div class="site-footer__location"><?php esc_attr_e( get_theme_mod('address', ''), 'webwolf' ); ?></div>
					<a href="<?php esc_attr_e( get_theme_mod('phone', ''), 'webwolf' ); ?>" class="site-footer__phone"><?php esc_attr_e( get_theme_mod('phone', ''), 'webwolf' ); ?></a>
				</div>
			</div>
		</div>
	</footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="site-footer__copyright-block">
                    <div class="site-footer__copyright">
                        <?php echo esc_html__( '© «Сольвент», 1997', 'webwolf' ); ?> - <?php echo date('Y'); ?>
                    </div>
                    <a class="site-footer__developers" target="_blank" rel="nofollow" href="http://web-wolf.ru/"><?php esc_html_e('Разработка сайта — WebWolf', 'webwolf') ?></a>
                </div>
            </div>
        </div>
    </div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
