const path = require('path');
const webpack = require("webpack");
const Promise = require('es6-promise').Promise;
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const BrowserSyncPlugin = require('browser-sync-webpack');
const autoprefixer = require('autoprefixer');
const config = require('./src/config');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const configuration = {
  entry: [
    './src/js/index.js',
    './src/scss/main.scss'
  ],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.min.js',
  },
  module: {
    rules : [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(sass|scss)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: [
                autoprefixer({
                  grid: "autoplace",
                  browsers:['> 1%', 'last 2 versions']
                })
              ],
              sourceMap: true
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'images/'
            }
          }
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
                outputPath: 'fonts/'
            }
        }]
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ]
      }
    ]
  },
  plugins: [
    new UglifyJSPlugin({
      uglifyOptions:
      {
          compress: {
              warnings: false
          },
          sourceMap: true
      }
    }),
    new OptimizeCSSAssetsPlugin(),
    new MiniCssExtractPlugin({
      filename: 'style.min.css'
    }),
    new webpack.ProvidePlugin({
      Promise: 'es6-promise-promise',
    }),
    new VueLoaderPlugin(),
    new BrowserSyncPlugin({
      proxy: config.url,
      host: 'localhost', 
      port: 3000,
      files: [
        '**/*.php',
        '**/*.scss',
        '**/*.js'
      ],
      reloadDelay: 0,
    },
    {
      reload: false
    })
  ]
};

module.exports = (env, options) => {
  const mode = options.mode === 'production';
  
  configuration.devtool = mode 
                        ? false
                        : 'eval-sourcemap';
  
  return configuration;
}