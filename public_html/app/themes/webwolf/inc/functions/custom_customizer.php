<?php

add_action('customize_register', function($customizer) {
    // Настройки сайта
    $customizer->add_section(
        'settings-site', array(
            'title' => 'Настройки сайта',
            'description' => 'Контактная информация на сайте',
            'priority' => 11,
        )
    );

    $customizer->add_setting( 'phone', array('default' => '') );
    $customizer->add_control(
        'phone',
        array(
            'label' => 'Телефон',
            'section' => 'settings-site',
            'type' => 'text'
        )
    );

    $customizer->add_setting( 'address', array('default' => '') );
    $customizer->add_control(
        'address',
        array(
            'label' => 'Адрес',
            'section' => 'settings-site',
            'type' => 'text'
        )
    );

    $customizer->add_setting( 'email', array('default' => '') );
    $customizer->add_control(
        'email',
        array(
            'label' => 'E-mail',
            'section' => 'settings-site',
            'type' => 'email'
        )
    );

    $customizer->add_setting( 'orderPhone', array('default' => '') );
    $customizer->add_control(
        'orderPhone',
        array(
            'label' => 'Модальное окно "Подать заявку"',
            'section' => 'settings-site',
            'type' => 'text',
            'description' => 'Вставьте шорткод для модального окна'

        )
    );
});