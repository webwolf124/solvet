<?php

/**
 * Button shortcodes
 */
add_action('admin_head', function () {
    // проверяем права пользователя - может ли он редактировать посты и страницы
    if (!current_user_can('edit_posts') && !current_user_can('edit_pages')) {
        return; // если не может, то и кнопка ему не понадобится, в этом случае выходим из функции
    }
    // проверяем, включен ли визуальный редактор у пользователя в настройках (если нет, то и кнопку подключать незачем)
    if ('true' == get_user_option('rich_editing')) {
        add_filter('mce_external_plugins', function ($plugin_array) {
            $plugin_array['custom_shortcode'] = get_stylesheet_directory_uri().'/src/js/custom_shortcode.js'; // true_mce_button - идентификатор кнопки
            return $plugin_array;
        });
        add_filter('mce_buttons', function ($buttons) {
            array_push($buttons, 'custom_shortcode'); // true_mce_button - идентификатор кнопки
            return $buttons;
        });
    }
});