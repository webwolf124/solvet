<?php

class PostWidget extends WP_Widget {
  function __construct() {
    // Запускаем родительский класс
    parent::__construct(
      '',
      __( 'Posts', 'sage' ),
      array( 'description' => 'Widget render posts.' )
    );
  }

	/**
	 * Вывод виджета во Фронт-энде
	 *
	 * @param array $args     аргументы виджета.
	 * @param array $instance сохраненные данные из настроек
	 */
	function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );

		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
    }
    $params = array (
      'showposts' => $instance['count'],
      'cat' => $instance['archive'],
    );
		$popular = new WP_Query( $params );
    if (have_posts()) : while ($popular->have_posts()) : $popular->the_post();
    ?>
    <a class="post-widget" href="<?php the_permalink(); ?>">
      <?php the_post_thumbnail(); ?>
      <div class="post-widget__title"><?php the_title(); ?></div>
      <span class="post-widget__time"><?php the_time('d F Y'); ?></span>
    </a>
    <?php
    endwhile; endif;

		echo $args['after_widget'];
	}

  /**
	 * Админ-часть виджета
	 *
	 * @param array $instance сохраненные данные из настроек
	 */
	function form( $instance ) {
    $title = @ $instance['title'] ?: __('Default title', 'sage');
    $count = @ $instance['count'] ?: 2;
    $category = get_categories();
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php esc_attr_e( $title ); ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'archive' ); ?>"><?php _e( 'Category' ); ?>:</label>
      <select name="<?php echo $this->get_field_name( 'archive' ); ?>" id="<?php echo $this->get_field_id( 'archive' ); ?>">
        <?php foreach($category as $key => $value): ?>
          <option value="<?php esc_attr_e( $value->term_id ); ?>" <?php if($value->term_id == $instance['archive']){ echo 'selected'; } ?> ><?php esc_attr_e( $value->name ); ?></option>
        <?php endforeach; ?>
      </select>
    </p>
    <p>
			<label for="<?php echo $this->get_field_id( 'count' ); ?>"><?php _e( 'Count' ); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'count' ); ?>" name="<?php echo $this->get_field_name( 'count' ); ?>" type="number" value="<?php esc_attr_e( $count ); ?>">
    </p>
		<?php 
  }
  
  /**
	 * Сохранение настроек виджета. Здесь данные должны быть очищены и возвращены для сохранения их в базу данных.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance новые настройки
	 * @param array $old_instance предыдущие настройки
	 *
	 * @return array данные которые будут сохранены
	 */
	function update( $new_instance, $old_instance ) {
		$instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['archive'] = ( ! empty( $new_instance['archive'] ) ) ? strip_tags( $new_instance['archive'] ) : '';
    $instance['count'] = ( ! empty( $new_instance['count'] ) ) ? strip_tags( $new_instance['count'] ) : '';

		return $instance;
	}

}

// регистрация Foo_Widget в WordPress
function register_post_widget() {
	register_widget( 'PostWidget' );
}
add_action( 'widgets_init', 'register_post_widget' );