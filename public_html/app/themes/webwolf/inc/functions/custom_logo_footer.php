<?php

function custom_logo_footer($wp_customize)
{
    $wp_customize->add_setting('header_logo', array(
            'default' => '',
            'sanitize_callback' => 'absint',
    ));

    $wp_customize->add_control(new WP_Customize_Media_Control($wp_customize, 'header_logo', array(
            'section' => 'title_tagline',
            'label' => 'Логотип в футоре'
    )));

    $wp_customize->selective_refresh->add_partial('header_logo', array(
            'selector' => '.header-logo',
            'render_callback' => function () {
                $logo = get_theme_mod('header_logo');
                $img = wp_get_attachment_image_src($logo, 'full');
                if ($img) {
                    return '<img src="' . $img[0] . '" alt="">';
                } else {
                    return '';
                }
            }
    ));
}
add_action('customize_register', 'custom_logo_footer');