<?php

add_action('init', 'register_stella_type_init'); // Использовать функцию только внутри хука init

function register_stella_type_init()
{
    $labels = array(
    'name' => 'Стеллы',
    'singular_name' => 'Стеллу', // админ панель Добавить->Функцию
    'add_new' => 'Добавить стеллу',
    'add_new_item' => 'Добавить новую стеллу', // заголовок тега <title>
    'edit_item' => 'Редактировать стеллу',
    'new_item' => 'Новая стелла',
    'all_items' => 'Все стеллы',
    'view_item' => 'Просмотр стеллы на сайте',
    'search_items' => 'Искать стеллу',
    'not_found' => 'Стелла не найдена.',
    'not_found_in_trash' => 'В корзине пусто.',
    'menu_name' => 'Стеллы', // ссылка в меню в админке
  );
    $args = array(
    'labels' => $labels,
    'public' => true,
    'show_in_rest' => true,
    'show_ui' => true, // показывать интерфейс в админке
    'has_archive' => false,
    'menu_position' => 7, // порядок в меню
    'supports' => array('title', 'thumbnail'),
  );
    register_post_type('stella', $args);
}
