<?php

add_action('init', 'register_sell_type_init'); // Использовать функцию только внутри хука init

function register_sell_type_init()
{
    $labels = array(
    'name' => 'Акции',
    'singular_name' => 'Акцию', // админ панель Добавить->Функцию
    'add_new' => 'Добавить акцию',
    'add_new_item' => 'Добавить новую акцию', // заголовок тега <title>
    'edit_item' => 'Редактировать акцию',
    'new_item' => 'Новая акция',
    'all_items' => 'Все акции',
    'view_item' => 'Просмотр акции на сайте',
    'search_items' => 'Искать акцию',
    'not_found' => 'Акция не найдена.',
    'not_found_in_trash' => 'В корзине нет акций.',
    'menu_name' => 'Акции', // ссылка в меню в админке
  );
    $args = array(
    'labels' => $labels,
    'public' => true,
    'show_in_rest' => true,
    'show_ui' => true, // показывать интерфейс в админке
    'has_archive' => false,
    'rewrite' => true,
    'hierarchical' => false,
    'query_var' => true,
    'menu_position' => 7, // порядок в меню
    'supports' => array('title', 'editor', 'author', 'thumbnail'),
    'taxonomies'  => array('sell_category')
  );
    register_post_type('sell', $args);
}

function register_sell_taxonomy() {
  register_taxonomy( 'sell_category', 'sell',
    array(
      'labels' => array(
        'name'              => 'Категории',
        'singular_name'     => 'Категория',
        'search_items'      => 'Найти категорию',
        'all_items'         => 'Категории',
        'edit_item'         => 'Редактировать категорию',
        'update_item'       => 'Обновить категорию',
        'add_new_item'      => 'Добавить категорию',
        'new_item_name'     => 'Имя категории',
        'menu_name'         => 'Категории',
        ),
      'hierarchical' => true,
      'sort' => true,
      'args' => array( 'orderby' => 'term_order' ),
      'show_admin_column' => true
      )
    );
}
add_action( 'init', 'register_sell_taxonomy' );
