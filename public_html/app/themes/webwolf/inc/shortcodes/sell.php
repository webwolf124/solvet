<?php
/**
 * Акции
 */
function shc_sell ( $atts ) {

  $options = shortcode_atts([
    'title'       => 'Акции',
		'showposts'   => 3,
    'button_text' => 'Все акции',
    'button_link' => '#'
  ], $atts);

    $params = array(
    'showposts'     => $options['showposts'],
    'post_type'     => 'sell',
    'sell_category' => 'aktsii'
  );

  $the_query = new WP_Query( $params );

  $posts = '
    <section id="service" class="service">
      <h2 class="site-title">
        <span>' . $options['title'] . '</span>
        <a href="' . $options['button_link'] . '" class="more-link">' . $options['button_text'] . '</a>
      </h2>
      <div class="service__grid">
    ';

  while($the_query->have_posts()) : $the_query->the_post();

    $posts .= '
    <div class="service__item">
      <a href="' . get_the_permalink() . '">
        ' . get_the_post_thumbnail(get_the_ID(), null, array( 'class' => 'service__image' )) . '
        <div class="service__title">' . get_the_title() . '</div>
        <div class="service__sell">' . get_field('sell') . '</div>
        <div class="service__date">' . get_field('date') . '</div>
      </a>
    </div>
    ';

  endwhile;

  $posts .= '
      </div>
    </section>
  ';

  wp_reset_query();

  return $posts;

  }

add_shortcode('shc_sell', 'shc_sell');
