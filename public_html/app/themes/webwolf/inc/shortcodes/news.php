<?php
/**
 * Акции
 */
function shc_news ( $atts ) {
  
  $options = shortcode_atts([
    'title'       => 'Новости компании',
		'showposts'   => 3,
    'button_text' => 'Все новости',
    'button_link' => '#'
  ], $atts);

    $params = array(
    'showposts' => $options['showposts'],
    'post_type' => 'post',
    'category'  => 'novosti'
  );

  $the_query = new WP_Query( $params );

  $posts = '
  <section class="news" id="news">
    <div class="container">
      <h2 class="site-title">
          <span>' . $options['title'] . '</span>
          <a href="' . $options['button_link'] . '" class="more-link">' . $options['button_text'] . '</a>
      </h2>
      <div class="news__grid">
    ';

  while($the_query->have_posts()) : $the_query->the_post();

    $posts .= '
    <div>
      <a href="' . get_the_permalink() . '" class="news__item">
        ' . get_the_post_thumbnail(get_the_ID(), null, array( 'class' => 'news__image' )) . '
        <div class="news__title">' . get_the_title() . '</div>
        <div class="news__time">' . get_the_time('d F Y') . '</div>
        <div class="news__exerpt">' . get_the_excerpt() . '</div>
      </a>
    </div>
    ';

  endwhile;

  $posts .= '
        </div>
      </div>
    </section>
  ';

  wp_reset_query();

  return $posts;

  }

add_shortcode('shc_news', 'shc_news');