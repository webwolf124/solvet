<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package webwolf
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	
<header class="site-header">
	<div class="site-header__main">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-4 col-md-6">
					<div class="site-header__logo">
						<?php the_custom_logo(); ?>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="site-header__title"><?php esc_html_e( 'Главный офис', 'webwolf' ); ?></div>
					<div class="site-header__location"><?php esc_attr_e( get_theme_mod('address', ''), 'webwolf' ); ?></div>
				</div>
				<div class="col-lg-5">
					<div class="site-header__info">
						<div class="site-header__contact">
							<a href="mailto:<?php esc_attr_e( get_theme_mod('email', ''), 'webwolf' ); ?>" class="site-header__email"><?php esc_attr_e( get_theme_mod('email', ''), 'webwolf' ); ?></a>
							<a href="tel:<?php esc_attr_e( get_theme_mod('phone', ''), 'webwolf' ); ?>" class="site-header__phone"><?php esc_attr_e( get_theme_mod('phone', ''), 'webwolf' ); ?></a>
						</div>
						<a data-fancybox data-src="#orderPhone" href="javascript:;" class="site-button"><img src="/app/themes/webwolf/src/images/message-solvent.svg" alt=""><?php esc_html_e( 'Задать вопрос', 'webwolf' ); ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<div class="site-header__menu">
	<div class="container">
		<div class="btn-menu">
			<span></span>
		</div>
		<nav class="site-header__main-navigation">
			<?php
				wp_nav_menu( array(
					'theme_location' => 'header',
					'menu_id'        => 'primary-menu',
				) );
			?>
		</nav>
	</div>
</div>

<div style="display: none;" id="orderPhone">
 <?php echo do_shortcode( get_theme_mod('orderPhone', 'Вставьте шорткод Contact Form 7') ); ?>
</div>