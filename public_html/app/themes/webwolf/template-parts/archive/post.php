<div>
  <a href="<?php the_permalink(); ?>" class="news__item">
    <?php the_post_thumbnail( null, array( 'class' => 'news__image' ) ); ?>
    <div class="news__title"><?php the_title() ?></div>
    <div class="news__time"><?php the_time('d F Y') ?></div>
    <div class="news__exerpt"><?php the_excerpt() ?></div>
  </a>
</div>