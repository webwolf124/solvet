<div class="service__item">
  <a href="<?php the_permalink(); ?>">
    <?php the_post_thumbnail( null, array( 'class' => 'service__image' ) ) ?>
    <div class="service__title"><?php the_title() ?></div>
    <div class="service__sell"><?php the_field('sell') ?></div>
    <div class="service__date"><?php the_field('date') ?></div>
  </a>
</div>
